#include <atomic>
#include <thread>

template <typename T, template <typename U> class Atomic = std::atomic>
class LockFreeQueue {
    struct Node {
        T element_{};
        Atomic<Node*> next_{nullptr};

        explicit Node(T element, Node* next = nullptr)
            : element_(std::move(element))
            , next_(next) {
        }

        explicit Node() {
        }
    };

public:
    explicit LockFreeQueue() {
        Node* dummy = new Node{};
        head_ = dummy;
        tail_ = dummy;
        deleted_ = dummy;
    }

    ~LockFreeQueue() {
        Node* to_delete = deleted_.load();
        while (to_delete) {
            Node* curr_vertex = to_delete;
            to_delete = curr_vertex->next_.load();
            delete curr_vertex;
        }
    }

    void Enqueue(T element) {
        working_threads_.fetch_add(1);
        Node* new_tail = new Node(element);
        Node* curr_tail = tail_.load();
        Node* null_vertex = nullptr;
        
        while (true) {
            curr_tail = tail_.load();
            if (!curr_tail->next_.load()) {
                if (curr_tail->next_.compare_exchange_strong(null_vertex, new_tail)) {
                    break;
                }
            } else {
                tail_.compare_exchange_strong(curr_tail, curr_tail->next_.load());
            }
        }

        tail_.compare_exchange_strong(curr_tail, new_tail);
        working_threads_.fetch_sub(1);
    }

    bool Dequeue(T& element) {
        working_threads_.fetch_add(1);
        while (true) {
            Node* curr_tail = tail_.load();
            Node* curr_head = head_.load();

            if (curr_head == curr_tail) {
                if (!curr_head->next_.load()) {
                    working_threads_.fetch_sub(1);
                    return false;
                } else {
                    tail_.compare_exchange_strong(curr_head, curr_head->next_.load());
                }
            } else {
                if (head_.compare_exchange_strong(curr_head, curr_head->next_.load())) {
                    element = curr_head->next_.load()->element_;
                    if (working_threads_.load() == 1) {
                        Node* old_deleted = deleted_.exchange(curr_head);
                        while (old_deleted != deleted_.load()) {
                            Node* curr_vertex = old_deleted;
                            old_deleted = curr_vertex->next_.load();
                            delete curr_vertex;
                        }
                    }
                    working_threads_.fetch_sub(1);
                    return true;
                }
            }
            
        }
    }

private:
    Atomic<Node*> head_{nullptr};
    Atomic<Node*> tail_{nullptr};
    Atomic<Node*> deleted_{nullptr};
    Atomic<size_t> working_threads_{0};
};
