#include <algorithm>
#include <atomic>
#include <condition_variable>
#include <forward_list>
#include <functional>
#include <mutex>
#include <shared_mutex>
#include <typeinfo>
#include <vector>

class RWMutex {
  public:
    RWMutex() 
        : writers_(0), readers_(0), writing_(false) {}

    RWMutex(const RWMutex&) = delete;
    RWMutex(RWMutex&&) = delete;

    void lock() {
        std::unique_lock<std::mutex> lock(lock_section_);
        writers_++;
        while (writing_ || readers_ > 0) {
            unlocked_.wait(lock);
        }
        writing_ = true;
    }

    void unlock() {
        std::unique_lock<std::mutex> unlock(lock_section_);
        writing_ = false;
        writers_--;
        unlocked_.notify_all();
    }

    void lock_shared() {
        std::unique_lock<std::mutex> lock(lock_section_);
        while (writers_ > 0) {
            unlocked_.wait(lock);
        }
        readers_++;
    }

    void unlock_shared() {
        std::unique_lock<std::mutex> lock(lock_section_);
        if (--readers_ == 0) {
            unlocked_.notify_all();
        }
    }

  private:
    std::mutex lock_section_;
    std::condition_variable unlocked_;
    size_t writers_;
    size_t readers_;
    bool writing_;
};

template <typename T, class Hash = std::hash<T> >
class StripedHashSet {
  public:
    StripedHashSet(size_t concurrency_level,
                   size_t growth_factor = 4,
                   long double max_load_factor = 0.75)
        : first_time_extend_(5),
        mutex_list_(concurrency_level),
        concurrency_level_(concurrency_level), 
        growth_factor_(growth_factor),
        max_load_factor_(max_load_factor) {
        std::forward_list<T> empty_list;
        buckets_ = std::vector<std::forward_list<T> >(concurrency_level_ * first_time_extend_,
                                                      empty_list);
        num_elements_.store(0);
    }

    bool Insert(const T& element) {
        const size_t hash_value = hash_function_(element);
        auto lock = LockBucket(hash_value);
        const size_t bucket_index = GetBucketIndex(hash_value);
        if (ContainsElement(element, bucket_index)) {
            return false;
        }
        
        buckets_[bucket_index].push_front(element);
        num_elements_.fetch_add(1);
        
        if (GetLoadFactor() >= max_load_factor_) {
            lock.unlock();
            ExpandTable();
        }
        return true;
    }

    bool Remove(const T& element) {
        const size_t hash_value = hash_function_(element);
        auto lock = LockBucket(hash_value);
        const size_t bucket_index = GetBucketIndex(hash_value);
        if (!ContainsElement(element, bucket_index)) {
            return false;
        }
        buckets_[bucket_index].remove(element);
        num_elements_.fetch_sub(1);
        return true;
    }

    bool Contains(const T& element) const {
        const size_t hash_value = hash_function_(element);
        auto lock = LockBucketForContains(hash_value);
        const size_t bucket_index = GetBucketIndex(hash_value);
        if (ContainsElement(element, bucket_index)) {
            return true;
        }
        return false;
    }

    size_t Size() const {
        return num_elements_.load();
    }

  private:
    size_t GetBucketIndex(const size_t hash_value) const {
        return hash_value % buckets_.size();
    }

    size_t GetStripeIndex(const size_t hash_value) const {
        return hash_value % concurrency_level_;
    }
    
    long double GetLoadFactor() const { 
        return static_cast<long double>(num_elements_.load()) / buckets_.size();
    }

    bool ContainsElement(const T& element, size_t bucket_index) const {
        auto index_of_element_in_bucket = std::find(buckets_[bucket_index].begin(), buckets_[bucket_index].end(), element);
        return index_of_element_in_bucket != buckets_[bucket_index].end();
    }

    void ExpandTable() {
        std::vector<std::unique_lock<RWMutex> > locks;
        locks.emplace_back(mutex_list_[0]);
        if (GetLoadFactor() >= max_load_factor_) {
            for (size_t i = 1; i < mutex_list_.size(); i++) {
                locks.emplace_back(mutex_list_[i]);
            }
            std::forward_list<T> empty_list;
            const size_t new_buckets_size = buckets_.size() * growth_factor_;
            std::vector<std::forward_list<T> > new_buckets_(new_buckets_size, empty_list);
            for (size_t i = 0; i < buckets_.size(); i++) {
                for (auto &element_in_bucket : buckets_[i]) {
                    const size_t hash_value = hash_function_(element_in_bucket);
                    new_buckets_[hash_value % new_buckets_size].push_front(element_in_bucket);
                }
            }
            buckets_.swap(new_buckets_);
        }
    }
    
    std::unique_lock<RWMutex> LockBucket(const size_t element_hash) const {
        return std::unique_lock<RWMutex>(mutex_list_[GetStripeIndex(element_hash)]);
    }

    std::shared_lock<RWMutex> LockBucketForContains(const size_t element_hash) const {
        return std::shared_lock<RWMutex>(mutex_list_[GetStripeIndex(element_hash)]);
    }

    std::vector<std::forward_list<T> > buckets_;
    const size_t first_time_extend_;
    mutable std::vector<RWMutex> mutex_list_;
    std::atomic<size_t> num_elements_;
    const size_t concurrency_level_;
    const size_t growth_factor_;
    const long double max_load_factor_;
    Hash hash_function_;
};

template <typename T> using ConcurrentSet = StripedHashSet<T>;
