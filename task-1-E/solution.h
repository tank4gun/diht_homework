#include <atomic>
#include <vector>
#include <thread>

std::size_t find_deg(std::size_t n) {
    std::size_t ans = 1;
    while (ans < n) {
        ans *= 2;
    }
    return ans;
}
    
struct node {
    bool child[2];
    std::size_t victim, blocked;
};

class TreeMutex {
public:
    TreeMutex(std::size_t n_threads) {
        tree_size_ = find_deg(n_threads) * 2;
        node test_node;
        test_node.child[0] = false;
        test_node.child[1] = false;
        test_node.victim = 0;
        test_node.blocked = 0;
        tree_.resize(tree_size_ * 2, test_node);
    }

    void lock(std::size_t current_thread) {
        // cur_pos - текущая позиция в дереве
        std::size_t cur_pos = tree_size_ / 2 + current_thread;
        std::size_t l_or_r = cur_pos % 2;  // левый или правый ребенок
        cur_pos = cur_pos / 2;
        while (cur_pos != 0) {
            tree_[cur_pos].child[l_or_r] = true;
            tree_[cur_pos].victim = l_or_r;
            while (tree_[cur_pos].child[1 - l_or_r] && tree_[cur_pos].victim == l_or_r) {
                std::this_thread::yield();
            }
            tree_[cur_pos].blocked = l_or_r;
            l_or_r = cur_pos % 2;
            cur_pos = cur_pos / 2;
        }
    }

    void unlock(std::size_t current_thread) {
        std::size_t cur_pos = 1;
        while (cur_pos < tree_size_ / 2 + current_thread) {
            cur_pos = cur_pos * 2 + tree_[cur_pos].blocked;
            tree_[cur_pos / 2].child[tree_[cur_pos / 2].blocked] = false;
        }
    }

private:
    std::vector<node> tree_;
    std::size_t tree_size_;
};
