#include <atomic>
#include <thread>

template <typename T>
class LockFreeStack {
    struct Node {
        T element_{};
        std::atomic<Node*> next_{nullptr};

        explicit Node() {}
        
        explicit Node(T element, Node* next = nullptr)
            : element_(std::move(element)),
            next_(next) {}
    };

public:
    explicit LockFreeStack() {}

    ~LockFreeStack() {
        Remove(top_.load());
        Remove(deleted_.load());
    }

    void Push(T element) {
        Node* curr_top = top_.load();
        Node* new_top = new Node{element};
        new_top->next_.store(curr_top);

        while (!top_.compare_exchange_weak(curr_top, new_top)) {
            new_top->next_.store(curr_top);
        }
    }

    bool Pop(T& element) {
        Node* curr_top = top_.load();
        while (true) {
            if (!curr_top) {
                return false;
            }
            if (top_.compare_exchange_strong(curr_top, curr_top->next_.load())) {
                element = curr_top->element_;
                InsertIntoList(curr_top);
                return true;
            }
        }
    }

private:
    void InsertIntoList(Node* curr_top) {
        Node* curr_deleted = deleted_.load();
        curr_top->next_.store(curr_deleted);

        while (!deleted_.compare_exchange_weak(curr_deleted, curr_top)) {
            curr_top->next_.store(curr_deleted);
        }
    }

    void Remove(Node* element) {
        while (element) {
            Node* curr_element = element;
            element = element->next_.load();
            delete curr_element;
        }
    }

    std::atomic<Node*> deleted_{nullptr};
    std::atomic<Node*> top_{nullptr};
};

template <typename T>
using ConcurrentStack = LockFreeStack<T>;
