#pragma once

#include "arena_allocator.h"

#include <atomic>
#include <limits>
#include <mutex>
#include <thread>

template <typename T>
struct ElementTraits {
    static T Min() {
        return std::numeric_limits<T>::min();
    }
    static T Max() {
        return std::numeric_limits<T>::max();
    }
};


class Spinlock {
  public:
    explicit Spinlock()
        : locked_(false) {}

    void Lock() {
        while (locked_.exchange(true)) {
            std::this_thread::yield();
        }
    }

    void Unlock() {
        locked_.store(false);
    }

    void lock() {
        Lock();
    }

    void unlock() {
        Unlock();
    }

  private:
    std::atomic<bool> locked_;
};


template <typename T>
class OptimisticLinkedSet {
  private:
    struct Node {
        T element_;
        std::atomic<Node*> next_;
        Spinlock lock_{};
        std::atomic<bool> marked_{false};

        Node(const T& element, Node* next = nullptr)
            : element_(element),
            next_(next) {}
    };

    struct Edge {
        Node* pred_;
        Node* curr_;

        Edge(Node* pred, Node* curr)
            : pred_(pred),
            curr_(curr) {}
    };

  public:
    explicit OptimisticLinkedSet(ArenaAllocator& allocator)
        : allocator_(allocator) {
        CreateEmptyList();
    }

    bool Insert(const T& element) {
        while (true) {
            Edge right_edge = Locate(element);
            auto lock_left = LockNodeFromEdge(right_edge.pred_);
            auto lock_right = LockNodeFromEdge(right_edge.curr_);
            if (Validate(right_edge)) {
                if (right_edge.curr_->element_ != element) {
                    Node* node_to_insert = allocator_.New<Node>(element, right_edge.curr_);
                    right_edge.pred_->next_.store(node_to_insert);
                    num_elements.fetch_add(1);
                    return true;
                } else {
                    return false;
                }
            }
        }
    }

    bool Remove(const T& element) {
        while (true) {
            Edge right_edge = Locate(element);
            auto lock_left = LockNodeFromEdge(right_edge.pred_);
            auto lock_right = LockNodeFromEdge(right_edge.curr_);
            if (Validate(right_edge)) {
                if (right_edge.curr_->element_ == element) {
                    right_edge.curr_->marked_.store(true);
                    right_edge.pred_->next_.store(right_edge.curr_->next_);
                    num_elements.fetch_add(-1);
                    return true;
                } else {
                    return false;
                }
            }
        }
    }


    bool Contains(const T& element) const {
        while (true) {
            Edge right_edge = Locate(element);
            auto lock_left = LockNodeFromEdge(right_edge.pred_);
            auto lock_right = LockNodeFromEdge(right_edge.curr_);
            if (Validate(right_edge)) {
                return right_edge.curr_->element_ == element && !right_edge.curr_->marked_.load();
            }
        }
    }

    size_t Size() const {
        return num_elements.load();
    }

  private:
    void CreateEmptyList() {
        head_ = allocator_.New<Node>(ElementTraits<T>::Min());
        head_->next_ = allocator_.New<Node>(ElementTraits<T>::Max());
        num_elements.store(0);
    }

    std::unique_lock<Spinlock> LockNodeFromEdge(Node* node) const {
        std::unique_lock<Spinlock> lock(node->lock_);
        return lock;
    }

    bool NotMarked(Node* node) const {
        return node->marked_.load() == false;
    }

    T GetNextNodeValue(const Node* curr_node) const {
        return curr_node->next_.load()->element_;
    }

    Edge Locate(const T& element) const {
        Node* curr_node = head_;
        while (GetNextNodeValue(curr_node) < element) {
            curr_node = curr_node->next_;
        }
        return Edge{curr_node, curr_node->next_};
    }

    bool Validate(const Edge& edge) const {
        if (edge.pred_->next_ == edge.curr_ && NotMarked(edge.pred_) && NotMarked(edge.curr_)) {
            return true;
        }
        return false;
    }

  private:
    ArenaAllocator& allocator_;
    Node* head_{nullptr};
    std::atomic<size_t> num_elements;
};

template <typename T> using ConcurrentSet = OptimisticLinkedSet<T>;
