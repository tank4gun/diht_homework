bool try_lock_tas_spinlock() {
    if (locked.test_and_set()) {
        return false;
    }
    return true;
}

bool try_lock_ticket_spinlock() {
    return next_ticket.compare_exchange_strong(owner_ticket, owner_ticket + 1);
}
