#include <condition_variable>
#include <atomic>
#include <mutex>

template <class ConditionVariable = std::condition_variable>
class CyclicBarrier {
 public:
    CyclicBarrier(size_t num_threads) {
        waiting_threads_ = 0;
        barrier_size_ = num_threads - 1;
        not_awake_ = true;
    }

    void Pass() {
        std::unique_lock<std::mutex> lock(mutex_);
        while (!not_awake_) {
            not_ready_cv_.wait(lock);
        }

        if (waiting_threads_ == barrier_size_) {
            not_awake_ = false;
            awake_waiting_threads_cv_.notify_all();
            if (waiting_threads_ == 0) {
                not_awake_ = true;
                not_ready_cv_.notify_all();
            }
        } else {
            waiting_threads_ += 1;
            while (not_awake_) {
                awake_waiting_threads_cv_.wait(lock);
            }
            if (waiting_threads_ == 1) {
                waiting_threads_ -= 1;
                not_awake_ = true;
                not_ready_cv_.notify_all();
            } else {
                waiting_threads_ -= 1;
            }
        }
    }

 private:
    int waiting_threads_;
    int barrier_size_;
    std::mutex mutex_;
    ConditionVariable awake_waiting_threads_cv_;
    ConditionVariable not_ready_cv_;
    bool not_awake_;
};
