#include <iostream>
#include <condition_variable>
#include <mutex>
#include <atomic>
#include <vector>

class Semaphore {
public:
    Semaphore() {
        num_ticket_.store(0);
    }

    void wait() {
        std::unique_lock<std::mutex> lock_the_door(mutex_);
        while (num_ticket_.load() == 0) {
            wait_for_notifying_cv_.wait(lock_the_door);
        }
        num_ticket_.store(0);
    }

    void signal() {
        num_ticket_.store(1);
        std::unique_lock<std::mutex> lock_the_door(mutex_);
        wait_for_notifying_cv_.notify_all();
    }
private:
    std::mutex mutex_;
    std::condition_variable wait_for_notifying_cv_;
    std::atomic<int> num_ticket_;
};

class Robot {
public:
    Robot(const std::size_t num_foots) {
        feet_ = std::vector<Semaphore>(num_foots);
        num_feet_ = num_foots;
        feet_[0].signal();
    }
    
    void Step(const std::size_t foot) {
        feet_[foot].wait();
        std::cout << "foot " << foot << std::endl;
        feet_[(foot + 1) % num_feet_].signal();
    }

private:
    std::vector<Semaphore> feet_;
    int num_feet_;
};
