#include <iostream>
#include <condition_variable>
#include <mutex>
#include <atomic>

class Semaphore {
public:
    Semaphore() {
        num_ticket_.store(0);
    }

    void wait() {
        std::unique_lock<std::mutex> lock(mutex_);
        while (num_ticket_.load() == 0) {
            wait_for_notifying_cv_.wait(lock);
        }
        num_ticket_.store(0);
    }

    void signal() {
        num_ticket_.store(1);
        std::unique_lock<std::mutex> lock(mutex_);
        wait_for_notifying_cv_.notify_all();
    }
private:
    std::mutex mutex_;
    std::condition_variable wait_for_notifying_cv_;
    std::atomic<int> num_ticket_;
};

class Robot {
public:
    Robot() {
        sem_left_.signal();
    }

    void StepLeft() {
        sem_left_.wait();
        std::cout << "left" << std::endl;
        sem_right_.signal();
    }

    void StepRight() {
        sem_right_.wait();
        std::cout << "right" << std::endl;
        sem_left_.signal();
    }

private:
    Semaphore sem_left_;
    Semaphore sem_right_;
};
