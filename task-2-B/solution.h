#include <cstdio>
#include <iostream>
#include <condition_variable>
#include <mutex>
#include <atomic>

class Robot {
public:
    Robot() {
        foot_.store(1);
    }
    
    void StepLeft() {
        std::unique_lock<std::mutex> lock(mutex_);
        while (foot_.load() != 1) {
            step_cv_.wait(lock);
        }
        std::cout << "left" << std::endl;
        foot_.store(2);
        step_cv_.notify_one();
    }

    void StepRight() {
        std::unique_lock<std::mutex> lock(mutex_);
        while (foot_.load() != 2) {
            step_cv_.wait(lock);
        }
        std::cout << "right" << std::endl;
        foot_.store(1);
        step_cv_.notify_one();
    }

private:
    std::condition_variable step_cv_;
    std::mutex mutex_;
    std::atomic<int> foot_;
};
