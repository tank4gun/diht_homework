#include <mutex>
#include <atomic>
#include <condition_variable>
#include <deque>
#include <stdexcept>
#include <utility>

template <class T, class Container = std::deque<T> >
class BlockingQueue {
public:
    explicit BlockingQueue(const size_t capacity) 
        : queue_size_(capacity), shutdowned_(false) {}

    void Put(T&& element) {
        std::unique_lock<std::mutex> lock(mutex_);
        while (queue_size_ == queue_.size() && !shutdowned_) {
            not_full_cv_.wait(lock);
        }
        if (shutdowned_) {
            throw std::runtime_error("Queue is already shutdowned!");
            return;
        }
        queue_.push_back(std::move(element));
        not_empty_cv_.notify_one();
    }

    bool Get(T& result) {
        std::unique_lock<std::mutex> lock(mutex_);
        while (queue_.empty() && !shutdowned_) {
            not_empty_cv_.wait(lock);
        }
        if (queue_.empty() && shutdowned_) {
            return false;
        }
        result = std::move(queue_.front());
        queue_.pop_front();
        not_full_cv_.notify_one();
        return true;
    }

    void Shutdown() {
        std::lock_guard<std::mutex> guard(mutex_);
        shutdowned_ = true;
        not_empty_cv_.notify_all();
        not_full_cv_.notify_all();
    }

 private:
    Container queue_;
    std::size_t queue_size_;
    bool shutdowned_;
    std::mutex mutex_;
    std::condition_variable not_empty_cv_;
    std::condition_variable not_full_cv_;
};
